class CreateGames < ActiveRecord::Migration[6.0]
  def change
    create_table :games do |t|
      t.string :name
      t.date :game_date
      t.string :game_time
      t.text :rules

      t.timestamps
    end
  end
end

class CreatePlayers < ActiveRecord::Migration[6.0]
  def change
    create_table :players do |t|
      t.integer :user_id
      t.integer :game_id
      t.float :game_score

      t.timestamps
    end
  end
end

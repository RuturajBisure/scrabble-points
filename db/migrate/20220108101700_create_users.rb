class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :phone_number
      t.string :email
      t.date :joining_date
      t.string :alias

      t.timestamps
    end
  end
end

## Problem defination/ Application description

A scrabble club requires a system to store members’ information and provide leader boards to show their top performing players.

For members we would want to store information such as the date they joined the club and their contact details.

All recorded scrabble games are head to head matches between 2 players, the player with the highest score at the end of the game wins.


## Requirements

This demo currently works with:

* Rails 5.2.x
* PostgreSQL

If you need help setting up a Ruby development environment, check out these [Rails OS X Setup Guide](https://mattbrictson.com/rails-osx-setup-guide).

## Run local installation
Clone git code
```
git clone https://gitlab.com/RuturajBisure/scrabble-points.git
bundle install
rails db:create db:migrate db:seed

And start server
```

## Database
```
create_table "games", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "name"
    t.date "game_date"
    t.string "game_time"
    t.text "rules"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "players", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "user_id"
    t.integer "game_id"
    t.float "game_score"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "name"
    t.string "phone_number"
    t.string "email"
    t.string "alias"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end
```

## Remaining development:
Scoreboard implementation is Still TBD
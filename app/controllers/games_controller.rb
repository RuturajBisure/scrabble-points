class GamesController < ApplicationController
  before_action :find_game, except: [:index, :new, :create]

  def index
    @games = Game.all
    respond_to do |format|
      format.json { render json: @games, status: 200 }
      format.html
    end
  end

  def show
    # games = @game.participants.pluck(:game_id).uniq
    # @win = 0; @los = 0
    # Game.where(id: games).each do |game|
    #   game.participants.order('score DESC').first.Game_id == @game.id ? @win +=1 : @los +=1
    # end
  end

  def new
    @game = Game.new
    2.times do
      player = @game.players.build
    end
  end

  def create
    @game = Game.new(game_params)

    respond_to do |format|
      if @game.save
        format.html { redirect_to games_path, notice: 'Game created.' }
        format.json { render :show, status: 200, location: @game }
      else
        format.html { render :new }
        format.json { render json: @game.errors, status: 404 }
      end
    end
  end

  def update
    respond_to do |format|
      if @game.update(game_params)
        format.html { redirect_to games_path, notice: 'Game updated.' }
        format.json { render :show, status: 200, location: @game }
      else
        format.html { render :edit }
        format.json { render json: @game.errors, status: 404 }
      end
    end
  end

  def destroy
    @game.destroy
    respond_to do |format|
      format.html { redirect_to games_url, notice: 'Game destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def find_game
      @game = Game.find(params[:id])
    end

    def game_params
      params.require(:game).permit(
        :name,
        :game_date,
        :game_time,
        :rules,
        players_attributes: [:user_id, :game_score]
      )
    end
end

class HomesController < ApplicationController
  def points_table
    user_ids = User.joins(:players).group('users.id').having("count(players.id)>=10")
    @leaderboard_users = User.where(id: user_ids).includes(:players).order("players.game_score desc").limit(10)
  end
end

class UsersController < ApplicationController
  before_action :find_user, except: [:index, :new, :create]

  def index
    @users = User.all
    respond_to do |format|
      format.json { render json: @users, status: 200 }
      format.html
    end
  end

  def show
    games = @user.players.pluck(:game_id).uniq
    @win = 0; @los = 0
    Game.where(id: games).each do |game|
      game.players.order('game_score DESC').first.user_id == @user.id ? @win +=1 : @los +=1
    end
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to users_path, notice: 'User created.' }
        format.json { render :show, status: 200, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: 404 }
      end
    end
  end

  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to users_path, notice: 'User updated.' }
        format.json { render :show, status: 200, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: 404 }
      end
    end
  end

  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def find_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(
        :email,
        :name,
        :phone_number,
        :alias,
      )
    end
end

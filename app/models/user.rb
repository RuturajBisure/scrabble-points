class User < ApplicationRecord
  validates :phone_number,
            numericality: true,
            length: { minimum: 10, maximum: 12,
            message: "minimum 10 and maximum 12 numbers allowded." }
  validates :email,
            presence: true,
            format: { with: /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i, 
                        message: 'is invalid'}
  validates :name,
            presence: true
 
  has_many :players, dependent: :destroy
end

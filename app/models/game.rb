class Game < ApplicationRecord
  has_many :players, dependent: :destroy

  accepts_nested_attributes_for :players
  validates_presence_of :name
end

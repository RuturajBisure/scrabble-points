Rails.application.routes.draw do
  root 'homes#index'
  resources :users, :games
  get '/points-table', :to => 'homes#points_table', as: :points_table
end
